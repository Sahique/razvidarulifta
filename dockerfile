# Use the official Python image as the base image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

RUN sudo apt-get update && \
    sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release && \
    sudo curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    sudo echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list && \
    sudo apt-get update && \
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io



# Install Flask and other dependencies
RUN pip install -r requirements.txt
RUN pip install cryptography

# Expose port 5000
EXPOSE 5000

# Define the command to run your application
CMD ["python", "app.py"]
