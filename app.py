# py -3 -m pip install Flask  (install libraries in python 3.9 syntax)

from flask import Flask, render_template, jsonify, request,redirect
#from flask_cors import CORS 
from flask import *
from flask_cors import CORS, cross_origin
import os
import docker 
import traceback

#/app/static/books
#/home/ubuntu/workspace/razvi/fatwa

# docker run -it -d --privileged --name razvi-app -v /home/ubuntu/workspace/razvi/fatwa:/app/static/books -p 5000:5000 rdi_app


app = Flask(__name__)
app.static_folder = 'static'
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# Get the list of DOCX files in the static/books folder
docx_folder = "./static/fatwa/" #os.path.join('static', 'books')
print(docx_folder)
#docx_files = [file for file in os.listdir(docx_folder) if file.endswith('.docx')]
docx_files = [file for file in os.listdir(docx_folder) ]
docx_files_without_extension = [os.path.splitext(file)[0] for file in os.listdir(docx_folder)]

docx_folder_books = "./static/books/" #os.path.join('static', 'books')
print("books: ",docx_folder_books)
#docx_files = [file for file in os.listdir(docx_folder) if file.endswith('.docx')]
try:
    docx_files_books = [file for file in os.listdir(docx_folder_books) ]
    docx_files_books_without_extension = [os.path.splitext(file)[0] for file in os.listdir(docx_folder_books)]
except:
    pass

@app.route('/')
def index():
    print(docx_files_without_extension)
    return render_template('index.html', docx_files=docx_files_without_extension)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/books')
def books():
    
    return render_template('books.html')

@app.route('/contact1')
def contact():
    return render_template('contact.html')

@app.route('/manifestation')
def manifestation():
    return render_template('manifestation.html')

@app.route('/fatwa')
def fatwa():
    docx_files = [file for file in os.listdir(docx_folder) ]
    docx_files_without_extension_local = [os.path.splitext(file)[0] for file in os.listdir(docx_folder)]
    print(docx_files_without_extension_local)
    return render_template('fatwa.html', docx_files=docx_files_without_extension_local)

@app.route('/load-docx/<filename>')
def load_docx(filename):
    filepath = os.path.join(docx_folder, filename+".docx")
    print(filepath)
    return send_file(filepath, as_attachment=True)

@app.route('/up')
def up():
    return render_template('upload.html')
  

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx', 'doc'}

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['POST'])
def upload():
    response = {"Ack": "failure"}

    if 'files[]' not in request.files or 'folder' not in request.form:
        return jsonify(response), 400  # Bad Request

    files = request.files.getlist('files[]')
    target_folder = request.form['folder']

    try:
        for file in files:
            if file and allowed_file(file.filename):
                upload_path = os.path.join(app.config['UPLOAD_FOLDER'], target_folder, file.filename)

                # Check if file already exists
                if os.path.exists(upload_path):
                    response = {"Ack": "file exists"}
                else:
                    os.makedirs(os.path.join(app.config['UPLOAD_FOLDER'], target_folder), exist_ok=True)
                    file.save(upload_path)
                    response = {"Ack": "success"}
        file_list = os.listdir(os.path.join(app.config['UPLOAD_FOLDER'], target_folder))
        app.logger.info(f"List of files after upload: {file_list}")
    except Exception as e:
        app.logger.error(e)
        response = {"Ack": "failed"}

    return jsonify(response)

@app.route('/login', methods=['POST'])
def login():
    response={"Ack": "failure","template_name":"","details":""}
    try:
        raw_data = request.data     # the data obtained here is in byte format ie [ b'{"username":"sahique","password":"","role":"Admin"}' ]
        
        data = json.loads(raw_data)
        print("inside ++>",data)    
        if (data['user']=="razviadmin" and data['password']=="darul@ifta1904"):
        #if (data['username']=="r" and data['password']=="d"):
            response['Ack']="success"
            response['template_name']="upload"

    except Exception as e:
        response['Ack']="Exception Occured: ",str(e)
        traceback.print_exc()
    print(response)
    return response    # 

if __name__ == '__main__':
    app.config['UPLOAD_FOLDER'] = 'static/'  # Default upload folder
    app.run(host='0.0.0.0', debug=True)
