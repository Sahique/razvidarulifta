import os
from flask import Flask, request, jsonify
from flask import Flask, render_template, jsonify, request,redirect

app = Flask(__name__)

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx', 'doc'}

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def books():
    return render_template('test.html')

@app.route('/upload', methods=['POST'])
def upload():
    response = {"Ack": "failure"}

    if 'files[]' not in request.files or 'folder' not in request.form:
        return jsonify(response), 400  # Bad Request

    files = request.files.getlist('files[]')
    target_folder = request.form['folder']

    try:
        for file in files:
            if file and allowed_file(file.filename):
                upload_path = os.path.join(app.config['UPLOAD_FOLDER'], target_folder, file.filename)

                # Check if file already exists
                if os.path.exists(upload_path):
                    response = {"Ack": "file exists"}
                else:
                    os.makedirs(os.path.join(app.config['UPLOAD_FOLDER'], target_folder), exist_ok=True)
                    file.save(upload_path)
                    response = {"Ack": "success"}
        file_list = os.listdir(os.path.join(app.config['UPLOAD_FOLDER'], target_folder))
        app.logger.info(f"List of files after upload: {file_list}")
    except Exception as e:
        app.logger.error(e)
        response = {"Ack": "failed"}

    return jsonify(response)

if __name__ == '__main__':
    app.config['UPLOAD_FOLDER'] = 'static/'  # Default upload folder
    app.run(debug=True)
